# Status LED for RaspberryPi

## Requirements

* 4x Low Current LED 3mm (I got two green and one yellow and one red)
* 4x 680 Ohm Resistor 0.6W ± 1%
* 5x Jumper Cable Female -Female
* Soldering iron and other equipment

## Setup

In order to use the LED's as status indicators, you need solder all the parts together. It begins with a resistor at each short end of the LED's. Form the other side of each resistor to one single contact. Now, the construction can be attached to the Pi. Therefore connect each LED to a free GPIO (I used 14, 15, 20, 21). The single contact with all the resistors needs to be connected to one GND pin on your Pi.

After attaching the LED's to the Pi, you need to apply the following config changes to your boot configuration file. Therefore open the boot config with `nano /boot/config.txt` and place the settings below the `[all]` section.
After the changes are saved, you need to restart the Pi to reload the config.

## Usage

* Green: Always on when Pi is on
* Green: Heartbeat blinking on load
* Yellow: Indicates on write access
* Red: Indicates CPU usage

## Config:

```
# power LED on gpio 14 (green)
enable_uart=1

# online indicator LED on gpio 15 (green)
dtoverlay=gpio-led,gpio=15,trigger=heartbeat,label=statusled1

# write activity LED on gpio 20 (yellow)
dtoverlay=gpio-led,gpio=20,trigger=mmc0,label=statusled2

# cpu activity LED on gpio 21 (red)
dtoverlay=gpio-led,gpio=21,trigger=cpu0,label=statusled3
```
