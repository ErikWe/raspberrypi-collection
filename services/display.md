# Display for RaspberryPi

## Requirements

* 1x Display with PCF8574 port expander
* 4x Jumper Cable Female -Female

## Setup

Connect the four ports of the expander to the RaspberryPi as follows:

* ND: Pin 6 (GND)
* VCC: Pin 4 (5V)
* SDA: Pin 3 (SDA)
* SCL: Pin 5 (SCL)

As soon as everything is connected correctly, the display should light up. Enable the *I2C* interface under section 3 inside the RaspberryPi configuration `sudo raspi-config`. Now, in order to get some information displayed, a bunch of libraries need to be installed. Therefore execute the following commands:
`sudo apt install python3-pip`
`sudo pip3 install RPLCD`
`sudo pip3 install smbus2`

Check if the display is detected by using `i2cdetect -y 1`. If the display is detected, run your Python script with `python3 /path/to/script.py`. The display should now show the information you want.
*HINT: if the display is still blank, check if there is a contrast settings wheel on the port expander. In my case the contrast was set to max by default...*

## Run as service
If you want your script to be started during boot and you want it to run in the background, then create a new service definition `sudo nano /etc/systemd/system/lcd.service` and add the following content:

```
[Unit]
Description=LCD Status Display
After=multi-user.target
[Service]
Type=simple
ExecStart=/usr/bin/python3 script.py
WorkingDirectory=/path/to/script
Restart=always
User=YOUR_USER
[Install]
WantedBy=multi-user.target
```

Finally, after saving the service definition, enable the service with `sudo systemctl enable lcd.service`.
